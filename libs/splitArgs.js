/**
 * So lets explain this code.
 *
 * You give it an array of splitters
 * Then it takes that array, along with the args, and turns it into a triple
 * Then it spits out the triple
 **/
var debug = require('debug')('splitter')
module.exports = function (_splitters, _args) {
  let _ids = []
  _splitters.forEach((_splitter) => {
    let _splitter_id = _args.indexOf(_splitter)
    if (_splitter_id > -1) _ids.push(_splitter_id)
  })
  debug('_ids', _ids)
  let _id = Math.min(_ids)
  debug('_id', _id)
  let _predicate = undefined
  _splitters.forEach((_splitter) => {
    if (_id === _args.indexOf(_splitter)) {
      _predicate = _splitter
      debug('predicate found', _predicate)
    }
  })
  if (_predicate) {
    let _args_split = _args.split(_predicate)
    let _subject = _args_split[0].trim()
    debug('subject found', _subject)
    let _object = _args_split[1].trim()
    debug('object found', _object)
    return {subject: _subject,predicate: _predicate,object: _object}
  }else {
    return 'Could not generate triple'
  }
}
