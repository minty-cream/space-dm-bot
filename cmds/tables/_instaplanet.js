"use strict"
var debug = require('debug')('_describe')

//80 x 20 map with a legend

let width=60
let height=20

let fill_map=function(x,y,key){
	//Probably a bad idea
	//Use a real decision maker
	if(map[y][x] === '.'){
		map[y][x] = key
	}
}
let legend_keys=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
let map=[]
for(let i=0;i<height;i++){
	let row=[]
		for(let j=0;j<width;j++){
			row.push('.')
		}
	map.push(row)
}

module.exports = function(args){
	let title=''
		let legend_num=0
		let legend=''
		for(title in tables){
			let arr=[]
				let amt=Math.floor(Math.random()*6)
				for(let i=0;i<amt;i++){
					legend = legend
						.concat(legend_keys[legend_num])
						.concat(': ')
						.concat(tables[title][Math.floor(Math.random()*tables[title].length)])
						.concat('\n')
						fill_map(Math.floor(Math.random()*width),Math.floor(Math.random()*height),legend_keys[legend_num])
						legend_num = legend_num + 1
				}
		}
	let out='\n```\n'
		for(let i=0;i<height;i++){
			for(let j=0;j<width;j++){
				out = out.concat(map[i][j])
			}
			out = out.concat('\n')
		}
	out=out
		.concat('```\n')
		.concat(legend)

		return out
}

let tables={
	'Interesting Feature':[
		'Ancient Temple',
		'Recently Unearthed Crashed Ship',
		'Blighted Lands',
		'Massive Sink Hole'
	],
	'Landmark 1':[
		'Lake/River',
		'Badlands',
		'Swamp',
		'Mud Flats',
		'Forest',
		'Canyon'
	],
	'Landmark 2':[
		'Mountain',
		'Forest',
		'Hills',
		'River',
		'Rocky Outcropping',
		'Desert'
	],
	'Population Center':[
		'Industrial',
		'Agricultural',
		'Science',
		'Education',
		'Scrap',
		'Settlement',
		'Religious'
	],
	'Point of Conflict':[
		'Two Groups are in Dispute Over Precious Material',
		'Mining Facility Has Collapsed',
		'Robots at Construction Facility Have Gone AWOL',
		'Hover Bike Race',
		'Smuggler Deal Going Down',
		'Smuggler Deal Getting Busted by Authorities',
		'Land Owners Being Bullied to Sell Their Land',
		'Facility That Uses Slave Labor',
		'Strike at Facility That has Erupted into Riots',
		'Military Combat Exercise'
	],
	'Interesting Feature 2':[
		'Formation of Large Crystals',
		'Massive Power Source',
		'Lone Arcology',
		'Isolated Religious Temple',
		'Isolated Research Facility',
		'Large Cliff With Expertly Carved Faces into the Rock Wall',
		'Hive of Recently Discovered Insects (or Animals)',
		'Colony of Exiled Aliens',
		'Abandoned Penal Colony',
		'Abandoned Research Facility'
	]
}
