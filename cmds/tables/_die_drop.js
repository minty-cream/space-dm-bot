"use strict"
var debug = require('debug')('_describe')

module.exports = {
	'Tech Level':[
		'Low',
		'Medium',
		'High',
		'Futuristic'
	],
	'Population':[
		'Abandoned– The society is gone- abandoned for whatever reason. There may be a few survivors here and there.',
		'Sparse– There are a few colonies throughout the planet. Most likely small outposts.  Probably no more than several tens of thousands inhabitants on the planet.',
		'Decent– A few colonies throughout the world, or a few cities, or one large city. Roughly a population of several million inhabitants.',
		'Busy– The planet is teeming with inhabitants. There are several large cities on the planet.   The planet has roughly a population of several hundred million to billions.',
		'Packed– The planet is bustling with inhabitants. There are several massive cities on the planet.  The population has roughly a population of several billion people.',
		'Metropolis– The planet is over-populated and may even be one giant city (GM’s call). The planet has a population of tens of billions of people.'
	],
	'Event 1':[
		'A sickness ran rampant through the population, killing roughly 20% of the inhabitants.',
		'A long running conflict has just come to an end and the world (or part of it) is celebrating.',
		'An unrecognized configuration of starship has crashed into the planet.',
		'The leader of the government has been assassinated. The world is in morning.',
		'The Mining Consortium desires to land on the planet and begin an invasive drilling process.',
		'A fleet of ships has arrived and formed a blockade around the planet for some unknown reason.',
		'The sun has been eclipsed for 1d6 days- a completely unnatural occurrence and the local populace does not know the cause behind this strange phenomenon.',
		'Tensions are running high amongst two rival factions. Tension and open hostility are high.'
	],
	'Event 2':[
		'Electric storms are raging through the area causing electronics to short out and shut down.',
		'A large group of pirates have congregated around the planet and are charging a tax to anyone wishing to set down.',
		'A strange fungus has wiped out many of the planet’s crops. The planet is in a state of famine.',
		'Several grizzly murders have occurred in the main city. People are wary of one another and look at everyone with suspicion.',
		'A group of ferocious aliens has set down on the planet and is engaged in a ritualistic hunting event. They don’t care if they hurt others in the process.',
		'Earthquakes rattle the area, destroying many buildings and damaging shuttleports.',
		'A strange breed of fuzzy rat is breeding at an alarming rate. In a few days the cities will be overrun with these cute, but annoying, rodents.',
		'Celebrations turn into riots when a beloved public figure is assassinated during the ceremonies.',
		'An old enemy of one of the PC’s shows up on the planet, and is looking for revenge.',
		'The planet is currently in a state of martial law- making traveling and gathering information extremely difficult.'
	],
	'Fascinating Structure':[
		'A crashed ship that has been turned into a tavern/inn.',
		'An ancient, large black obelisk that rests just outside of the city.',
		'A government building that is built on massive floating lily pads.',
		'An inverted pyramid built into the ground by ancient hands.',
		'A building made from faintly glowing crystal.',
		'An ancient religious temple intricately carved from limestone.',
		'The penal colony is built on a massive crab walker and moves around the planet.',
		'Building is a spaceship that hovers in the sky.',
		'Structure is sculpted from a comet that crashed on the planet thousands of years ago.',
		'Building of soft angles and several stories is constructed completely out of gold.'
	],
	'Structures':[
		'Prefabricated structures created of metal and fiber-plastic.',
		'Structures are made of stone and mud.',
		'Many of the structures are created from storage/shipping containers.',
		'The structures are created from non-functioning/redesigned starships.',
		'Structures are created from living plant matter.',
		'All structures have a conch shell-like construction.',
		'Buildings are huge arcologies.',
		'Structures are made of a bronze metal that is warm to the touch.',
		'Structures are round glass buildings.',
		'The inside of the buildings are labyrinthine.',
		'The buildings are covered in solar panels.',
		'The buildings are all built on large metal or stone stilts.'
	],
	'Environments':[
		'Desert planet.',
		'Ice planet.',
		'Lush jungle.',
		'Planet is covered in massive mushrooms.',
		'Patches of the planet are barren rock.',
		'Toxic and covered in a miasma. All cities are built in protective domes.',
		'Giant crystals grow out of the ground and hum when lifeforms are near.',
		'Planet is a gas cloud. All buildings are on floating rocks.',
		'Entire planet is an ocean. All buildings are in protective domes or built above the sea on floating barges.',
		'Swamp planet.',
		'Volcanic planet.',
		'Planet is toxic and barren. All life lives under the planet crust.',
		'Planet is covered in massive sand storms.',
		'Planet is swampy. It rains 90% of its cycle.',
		'Arid planet.',
		'Tundra planet.',
		'Planet of lush vegetation. The vegetation forms a conscious hive mind.',
		'Artificially built planet. No one knows who constructed this wonder.',
		'Planet is a living being and is covered in fleshy membranes.'
	],
	'Planet Name': [
		'Tryklar',
		'Abtoss One',
		'Z-7665H',
		'Hyphron',
		'Haven',
		'Yzrak',
		'Port Myth',
		'Nazpar',
		'D-5517',
		'Poplar',
		'Ghystar',
		'Byztine',
		'Klaxon',
		'Beta',
		'P-4832Z',
		'Rockbalt',
		'Carcass',
		'Victory',
		'Whendon',
		'Lythblat'
			]
}
