"use strict"
var debug = require('debug')('_describe')

module.exports = function (_args) {
	debug('_args', _args)
		let die_drop=require(__dirname + '/tables/_die_drop')
		let instaplanet=require(__dirname + '/tables/_instaplanet')
		let output=['','']
		let title=[]
		for(title in die_drop){
			let result=die_drop[title][Math.floor(Math.random()*die_drop[title].length)]
				output[0]=output[0]
				.concat(title)
				.concat(': ')
				.concat(result)
				.concat('\n')
		}
	output[1]='GM, Please describe the planet in more detail using this map for inspiration.'
		output[1]=output[1].concat(instaplanet())
		return output
}
